package com.walton.luke.spoons;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class basketActivity extends AppCompatActivity {

    Boolean extended;
    LinearLayout t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);
        t = findViewById(R.id.thing);
        extended = false;
    }

    public void buy(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Select Payment")
                .setMessage("Not implemented for demo")
                .setPositiveButton("Pay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        loadTracker();
                    }
                })
                .setNegativeButton("Don't pay", null)
                .show();
    }

    public void settings(View view) {
        startActivity(new Intent(this, settingsActivity.class));
    }

    private void loadTracker() {
        startActivity(new Intent(this, tracker.class));
    }

    public void extenda(View view) {
        if (extended) {
            t.setVisibility(View.GONE);
        } else {
            t.setVisibility(View.VISIBLE);
        }
        extended = !extended;
    }
}
