package com.walton.luke.spoons;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void order(View view) {
        Intent i = new Intent(this, orderActivity.class);
        startActivity(i);
    }

    public void diets(View view) {
        Intent i = new Intent(this, dietActivity.class);
        startActivity(i);
    }

    public void location(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Change location")
                .setMessage("Select your location:")
                .setPositiveButton("change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setNegativeButton("Don't change", null)
                .show();
    }
}
