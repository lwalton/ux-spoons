package com.walton.luke.spoons;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class orderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
    }

    public void settings(View view) {
        startActivity(new Intent(this, settingsActivity.class));
    }

    public void order2(View view){ startActivity(new Intent(this, order2Activity.class));}
}
