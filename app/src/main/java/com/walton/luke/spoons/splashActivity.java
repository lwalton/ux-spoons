package com.walton.luke.spoons;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class splashActivity extends AppCompatActivity {

    private SharedPreferences sp;
    private static final String WIFI = "wifi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sp = getSharedPreferences("spoons", Context.MODE_PRIVATE);
        if (sp.contains(WIFI)) {
            if (!sp.getBoolean(WIFI, false)) {
                loadMenu();
            }
        }
    }

    public void connect(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Connect to Wetherspoons")
                .setMessage("Our new app means you no longer need to know your table number! Simply connect to our WiFi and we'll do the rest!")
                .setPositiveButton("Let's do this!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sp.edit().putBoolean(WIFI, true).apply();
                        loadMenu();
                    }
                })
                .setNegativeButton("No thanks", null)
                .show();
    }

    private void loadMenu() {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void nope(View view) {
        startActivity(new Intent(this, tableset.class));

    }
}
