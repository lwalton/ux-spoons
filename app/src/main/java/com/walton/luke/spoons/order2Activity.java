package com.walton.luke.spoons;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class order2Activity extends AppCompatActivity {

    private TextView bdropdown, cdropdown;
    private boolean visible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order2);
        bdropdown = findViewById(R.id.burger);
        cdropdown = findViewById(R.id.chicken);
    }

    public void extendB(View view) {
        if (visible) {
            bdropdown.setVisibility(View.GONE);
        } else {
            bdropdown.setVisibility(View.VISIBLE);
        }
        visible = !visible;
    }

    public void extendC(View view) {
        if (visible) {
            cdropdown.setVisibility(View.GONE);
        } else {
           cdropdown.setVisibility(View.VISIBLE);
        }
        visible = !visible;
    }

    public void basket(View view) {
        startActivity(new Intent(this, basketActivity.class));
    }
}
