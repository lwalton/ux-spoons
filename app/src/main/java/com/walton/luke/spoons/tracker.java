package com.walton.luke.spoons;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class tracker extends AppCompatActivity {

    private static int imageNumber;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);
        imageView = findViewById(R.id.imagev);
        imageNumber = 0;
    }

    public void swapImage(View view) {
        imageNumber++;
        if (imageNumber > 6) imageNumber = 0;
        switch (imageNumber) {
            case 0:
                imageView.setImageDrawable(getDrawable(R.drawable.a0));
                break;
            case 1:
                imageView.setImageDrawable(getDrawable(R.drawable.a1));
                break;
            case 2:
                imageView.setImageDrawable(getDrawable(R.drawable.a2));
                break;
            case 3:
                imageView.setImageDrawable(getDrawable(R.drawable.a3));
                break;
            case 4:
                imageView.setImageDrawable(getDrawable(R.drawable.a4));
                break;
            case 5:
                imageView.setImageDrawable(getDrawable(R.drawable.a5));
                break;

        }
    }

    public void homee(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
