# UX CW, Wetherspoons app experience overhaul prototype, Luke Walton, 867775@swansea.ac.uk

App has only been tested on OnePlus 6, and has not been designed to work on any other phones. A video of the app working is available in the resources folder. This is a generic android app and so should work on other devices, but the UI has been hardcoded for the OnePlus 6, and so many screens may not work on other phones. The UI is not the point of this app, but is simply there to show the new experience created for the ordering process at Wetherspoons
